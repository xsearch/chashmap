#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "chashmap.h"

#define TOTAL_TESTS 2
#define SEED 11155
#define SIZE 1 << 28
#define NUMVALS 1 << 20

void init_vals(int32_t vals[NUMVALS])
{
    int i;
    
    srand(SEED);
    for (i = 0; i < NUMVALS; i++) {
        vals[i] = rand() % (SIZE / 2);
    }
}

int test_put_vals(chashmap *hashmap, int32_t vals[NUMVALS])
{
    int i, rc;

    for (i = 0; i < NUMVALS; i++) {
        rc = chashmap_put(hashmap, vals[i], vals[i]);
        if (rc != CHASHMAP_RET_OK) {
            printf("BUG: chashmap_put returned %d, while expecting %d!\n",
                    rc, CHASHMAP_RET_OK);
            return 1;
        }
    }

    return 0;
}

int test_get_vals(chashmap *hashmap, int32_t vals[NUMVALS])
{
    int i, rc, val;

    for (i = 0; i < NUMVALS; i++) {
        val = 0;
        rc = chashmap_get(hashmap, vals[i], &val);
        if (rc != CHASHMAP_RET_OK) {
            printf("BUG: chashmap_get returned %d, while expecting %d!\n",
                    rc, CHASHMAP_RET_OK);
            return 1;
        }
        if (val != vals[i]) {
            printf("BUG: chashmap_get got %d, while expecting %d!\n",
                    val, vals[i]);
            return 1;
        }
    }

    return 0;
}

int test_default(const char teststr[])
{
    int rc;
    int32_t *vals;
    chashmap hashmap;

    printf("%s\n", teststr);
    vals = (int32_t*) malloc(sizeof(int32_t) * NUMVALS);
    init_vals(vals);
    
    rc = chashmap_init_default(&hashmap, SIZE); 
    if (rc != CHASHMAP_RET_OK) {
        printf("BUG: chashmap_init returned %d, while expecting %d!\n",
                rc, CHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: chashmap_init successful.\n");

    rc = test_put_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: chashmap_put successful.\n");

    rc = test_get_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: chashmap_get successful.\n");

    rc = chashmap_destroy(&hashmap);
    if (rc != CHASHMAP_RET_OK) {
        printf("BUG: chashmap_destroy returned %d, while expecting %d!\n",
                rc, CHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: chashmap_destroy successful.\n");

    free(vals);
    printf("PASS: test_custom passed!\n");
    return 1;
}

int32_t test_hash(chashmap *hashmap, int32_t val)
{
    return val % (hashmap->size / 2);
}

int test_custom(const char teststr[])
{
    int rc;
    int32_t *vals;
    chashmap hashmap;

    printf("%s\n", teststr);
    vals = (int32_t*) malloc(sizeof(int32_t) * NUMVALS);
    init_vals(vals);
    
    rc = chashmap_init(&hashmap, SIZE, test_hash); 
    if (rc != CHASHMAP_RET_OK) {
        printf("BUG: chashmap_init returned %d, while expecting %d!\n",
                rc, CHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: chashmap_init successful.\n");

    rc = test_put_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: chashmap_put successful.\n");

    rc = test_get_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: chashmap_get successful.\n");

    rc = chashmap_destroy(&hashmap);
    if (rc != CHASHMAP_RET_OK) {
        printf("BUG: chashmap_destroy returned %d, while expecting %d!\n",
                rc, CHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: chashmap_destroy successful.\n");

    free(vals);
    printf("PASS: test_custom passed!\n");
    return 1;
}

int main(int argc, char *argv[argc])
{
    int num_tests = 0;

    printf("Running CHashMap tests ...\n");
    num_tests += test_default("Testing hashmap with default hash function.");
    num_tests += test_custom("Testing hashmap with custom hash function.");
    printf("Finished running tests. %d/%d tests have passed.\n", 
            num_tests, TOTAL_TESTS);

    return 0;
}
