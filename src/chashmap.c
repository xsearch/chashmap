#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "chashmap.h"

int32_t chashmap_default_hash(chashmap *hashmap, int32_t val)
{
    return val % hashmap->size;
}

int chashmap_init_default(chashmap *hashmap, int32_t size)
{
    int i, rc;
    
    if (size <= 0) {
        printf("ERROR: hashmap must have a size greater than 0!\n");
        return CHASHMAP_RET_ERR;
    }
    hashmap->size = size;

    hashmap->hash = chashmap_default_hash;

    hashmap->map = (centry*) malloc(sizeof(centry) * size);
    if (hashmap->map == NULL) {
        printf("ERROR: could not allocated hashmap!\n");
        return CHASHMAP_RET_ERR;
    }

    for (i = 0; i < size; i++) {
        rc = pthread_spin_init(&(hashmap->map[i].spinlock),
                PTHREAD_PROCESS_PRIVATE);
        if (rc) {
            printf("ERROR: could not initialize a mutex!\n");
            return CHASHMAP_RET_ERR;
        }
    }

    return CHASHMAP_RET_OK;
}

int chashmap_init(chashmap *hashmap, int32_t size,
        int32_t (*hash)(chashmap*, int32_t))
{
    int i, rc;

    if (size <= 0) {
        printf("ERROR: hashmap must have a size greater than 0!\n");
        return CHASHMAP_RET_ERR;
    }
    hashmap->size = size;

    hashmap->hash = hash;

    hashmap->map = (centry*) malloc(sizeof(centry) * size);
    if (hashmap->map == NULL) {
        printf("ERROR: could not allocated hashmap!\n");
        return CHASHMAP_RET_ERR;
    }

    for (i = 0; i < size; i++) {
        rc = pthread_spin_init(&(hashmap->map[i].spinlock),
                PTHREAD_PROCESS_PRIVATE);
        if (rc) {
            printf("ERROR: could not initialize a mutex!\n");
            return CHASHMAP_RET_ERR;
        }
    }

    return CHASHMAP_RET_OK;
}


int chashmap_put(chashmap *hashmap, int32_t key, int32_t val)
{
    int k;

    k = hashmap->hash(hashmap, key);
    pthread_spin_lock(&(hashmap->map[k].spinlock));
    hashmap->map[k].value = val;
    pthread_spin_unlock(&(hashmap->map[k].spinlock));

    return CHASHMAP_RET_OK;
}

int chashmap_get(chashmap *hashmap, int32_t key, int32_t *val)
{
    int k;

    k = hashmap->hash(hashmap, key);
    *val = hashmap->map[k].value;

    return CHASHMAP_RET_OK;
}

int chashmap_destroy(chashmap *hashmap)
{
    int i, rc;

    for (i = 0; i < hashmap->size; i++) {
        rc = pthread_spin_destroy(&(hashmap->map[i].spinlock));
        if (rc) {
            printf("ERROR: could not destroy a mutex!\n");
            return CHASHMAP_RET_ERR;
        }
    }
    hashmap->size = -1;
    hashmap->hash = chashmap_default_hash;
    free(hashmap->map);
    hashmap->map = 0;

    return CHASHMAP_RET_OK;
}
