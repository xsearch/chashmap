#ifndef CHASHMAP_H
#define CHASHMAP_H

#include <stdint.h>
#include <pthread.h>

#define CHASHMAP_RET_OK 0
#define CHASHMAP_RET_ERR -1

typedef struct centry {
    int32_t value;
    pthread_spinlock_t spinlock;
} centry;

typedef struct chashmap {
    centry *map;
    int32_t size;
    int32_t (*hash)(struct chashmap*, int32_t);
} chashmap;

extern int chashmap_init_default(chashmap *hashmap, int32_t size);
extern int chashmap_init(chashmap *hashmap, int32_t size,
        int32_t (*hash)(chashmap*, int32_t));
extern int chashmap_put(chashmap *hashmap, int32_t key, int32_t val);
extern int chashmap_get(chashmap *hashmap, int32_t key, int32_t *val);
extern int chashmap_destroy(chashmap *hashmap);

#endif
